import { Guid } from './Guid';

export default interface DietaryRestriction {
  DietaryRestrictionId: Guid;
  Description: string;
}
