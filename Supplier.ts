import { Guid } from './Guid';

export default interface Supplier {
  SupplierId: Guid;
  Name: string;
  Description: string;
  VideoId: string;
  SupplierTypeId: Guid;
  Video: string;
}
