class Equipment {
  constructor(hardware, locations, taxDistricts) {
    this.hardware = hardware || [];
    this.locations = locations || [];
    this.taxDistricts = taxDistricts || [];
    if (hardware && locations && taxDistricts) {
      this.hardwareFlat = this.hardware.map((h) => {
        const flat = h;
        const location = this.locations.find(l => l.LocationId === h.LocationId);
        flat.address = location.Address;
        flat.latitude = location.Latitude;
        flat.longitude = location.Longitude;
        flat.description = location.Description;
        flat.tax_rate = this.taxDistricts
          .find(t => t.TaxDistrictId === location.TaxDistrictId).TaxRate;
        return flat;
      });
    }
  }

  getHardware(hardwareId, withRelationships) {
    const hardware = this.hardware.find(h => h.HardwareId === hardwareId);
    if (withRelationships) {
      hardware.location = this.locations.find(l => l.LocationId === hardware.LocationId);
      hardware.taxDistrict = this.taxDistricts
        .find(t => t.TaxDistrictId === hardware.location.TaxDistrictId);
    }
    return hardware;
  }

  getHardwareByLocationIds(locationIds) {
    return this.hardware.filter(h => locationIds.includes(h.LocationId));
  }

  getHardwareFlat(hardwareId) {
    if (hardwareId) {
      return this.hardwareFlat.find(h => h.HardwareId === hardwareId);
    }
    return this.hardwareFlat;
  }
}

export default Equipment;
