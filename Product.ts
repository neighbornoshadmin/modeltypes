import { Guid } from './Guid';

export default interface Product {
  ProductId: Guid;
  CategoryId: Guid;
  SupplierId: Guid;
  Sku: string;
  ProductCode: string;
  Name: string;
  Active: boolean;
  Description: string;
  Price: number;
  Picture: string;
  PictureFilename: string;
  Label: string;
  LabelFilename: string;
  DietaryRestrictionIds: Guid[];
  LabelRatio: number;
}
