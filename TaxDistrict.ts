import { Guid } from './Guid';

export default interface TaxDistrict {
  TaxDistrictId: Guid;
  Name: string;
  TaxRate: number;
}
