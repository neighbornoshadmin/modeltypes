enum PaymentStatus {
  None = 0,
  Authorized = 1, // Received payment authorization
  Settled = 2, // Payment charge completed
  NoAuth = -1, // Payment authorization declined
}

export default PaymentStatus;
