import { Guid } from './Guid';

export default interface Stock {
  ProductId: Guid;
  Quantity: number;
}
