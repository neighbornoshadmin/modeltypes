import { Guid } from './Guid';

export default interface Category {
  CategoryId: Guid;
  TaxableRate: number;
  /**
   * @deprecated
   */
  Section: boolean; // True if food, false if beverage
  Name: string;
}
