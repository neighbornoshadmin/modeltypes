import { Guid } from './Guid';

class ReservationProduct {
  ProductId: Guid;

  Vended: boolean;

  VendedAt: Date | string | null;

  Price: number;

  TaxableRate: number;
}

export default ReservationProduct;
