import { Guid } from './Guid';

export type CartItem = {
  productId: Guid,
  quantity: number,
};
