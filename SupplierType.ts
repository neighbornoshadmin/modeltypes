import { Guid } from './Guid';

export default interface SupplierType {
  SupplierTypeId: Guid;
  Description: string;
}
