import CartState from '../../reducers/cart/CartState';
import { Guid } from './Guid';

export default class Cart {
  constructor(state: CartState) {
    this.items = state.items;
    this.selectedHardware = state.selectedHardware;
  }

  get isEmpty(): boolean {
    return this.items.length < 1;
  }

  get count(): number {
    return this.items.length;
  }

  get total(): number {
    return this.items.reduce((a, i) => a + i.Price, 0);
  }

  hasProduct(productId: Guid): boolean {
    return this.items.some((i) => i.ProductId === productId);
  }

  productCount(productId: Guid): number {
    return this.items.filter((i) => i.ProductId === productId).length;
  }
}
