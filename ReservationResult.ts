enum ReservationResult {
  Success = 0,
  Fail = 1,
  Partial = 2,
}

export default ReservationResult;
