class ReservationPayment {
  CorrelationId: string;

  TransactionStatus: string;

  TransactionId: string;

  TransactionType: string;

  TransactionTag: string;

  Amount: string;
}

export default ReservationPayment;
