enum ReservationSource {
  App = 0,
  VendingHardware = 1,
}

export default ReservationSource;
