import { Nominal } from 'simplytyped';

export type Guid = Nominal <string, 'Guid'>;

export const NIL = '00000000-0000-0000-0000-000000000000' as Guid;
