enum ReservationStatus {
  New = 0,
  VendingStarted = 1, // At least one item vended
  VendingFinished = 2, // All items vended
  Closed = 3, // Tipping window closed
  Completed = 4, // Final status
  Expired = 5, // Timed out
  Canceled = 6, // User canceled
  Killed = 7, // Admin canceled
}

export default ReservationStatus;
