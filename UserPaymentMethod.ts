import { Guid } from './Guid';
import { PaymentMethodToken } from './PaymentMethodToken';

export interface UserPaymentMethod {
  PaymentId: Guid;
  CardNumber: string;
  Token: PaymentMethodToken;
}
