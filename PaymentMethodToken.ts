export interface PaymentMethodToken {
  CardholderName: string;
  ExpirationDate: string;
  Type: string;
  Value: string;
}
