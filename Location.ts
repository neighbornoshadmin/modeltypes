import { Guid } from './Guid';

export default interface Location {
  LocationId: Guid;
  TaxDistrictId: Guid;
  Name: string;
  Address: string;
  Notes: string;
  Latitude: number;
  Longitude: number;
}
