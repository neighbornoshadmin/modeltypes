import Equipment from '../Equipment';

let equipment;
let hardware;
let locations;
let taxDistricts;

beforeEach(() => {
  hardware = [
    { hardware_id: 0, location_id: 0 },
    { hardware_id: 1, location_id: 1 },
  ];
  locations = [
    { location_id: 0, name: 'location 0', tax_district_id: 0, latitude: 0.5, longitude: 0.5, address: 'address 0' },
    { location_id: 1, name: 'location 1', tax_district_id: 1, latitude: 1.5, longitude: 1.5, address: 'address 1' },
  ];
  taxDistricts = [
    { tax_district_id: 0, name: 'tax district 0', tax_rate: 0.8 },
    { tax_district_id: 1, name: 'tax district 1', tax_rate: 0.7 },
  ];
  equipment = new Equipment(hardware, locations, taxDistricts);
});

describe('Equipment', () => {
  describe('ctor', () => {
    it('should set hardware', () => {
      expect(equipment.hardware).toEqual(hardware);
    });
    it('should create empty array if hardware null', () => {
      equipment = new Equipment(null, locations, taxDistricts);
      expect(equipment.hardware).toEqual([]);
    });
    it('should create empty array if hardware undefined', () => {
      equipment = new Equipment(undefined, locations, taxDistricts);
      expect(equipment.hardware).toEqual([]);
    });
    it('should set locations', () => {
      expect(equipment.locations).toEqual(locations);
    });
    it('should create empty array if locations null', () => {
      equipment = new Equipment(hardware, null, taxDistricts);
      expect(equipment.locations).toEqual([]);
    });
    it('should create empty array if locations undefined', () => {
      equipment = new Equipment(hardware, undefined, taxDistricts);
      expect(equipment.locations).toEqual([]);
    });
    it('should set taxDistricts', () => {
      expect(equipment.taxDistricts).toEqual(taxDistricts);
    });
    it('should create empty array if taxDistricts null', () => {
      equipment = new Equipment(hardware, locations, null);
      expect(equipment.taxDistricts).toEqual([]);
    });
    it('should create empty array if taxDistricts undefined', () => {
      equipment = new Equipment(hardware, locations, undefined);
      expect(equipment.taxDistricts).toEqual([]);
    });
  });
  describe('getHardware', () => {
    it('should return correct hardware', () => {
      expect(equipment.getHardware(1)).toEqual(hardware[1]);
    });
    it('should exclude location if withRelationships false', () => {
      expect((equipment.getHardware(1, false)).location).toBeUndefined();
    });
    it('should inlude location if withRelationships true', () => {
      expect((equipment.getHardware(1, true)).location).toEqual(locations[1]);
    });
    it('should exclude taxDistrict if withRelationships false', () => {
      expect((equipment.getHardware(1, false)).taxDistrict).toBeUndefined();
    });
    it('should include taxDistrict if withRelationships true', () => {
      expect((equipment.getHardware(1, true)).taxDistrict).toEqual(taxDistricts[1]);
    });
  });
  describe('getHardwareByLocations', () => {
    it('should include hardware in selected locations', () => {
      expect(equipment.getHardwareByLocationIds([0])).toEqual([hardware[0]]);
    });
  });
  describe('getHardwareFlat', () => {
    it('should include the latitide', () => {
      expect(equipment.getHardwareFlat()[0].latitude).toEqual(locations[0].latitude);
    });
    it('should include the longitude', () => {
      expect(equipment.getHardwareFlat()[0].longitude).toEqual(locations[0].longitude);
    });
    it('should include the address', () => {
      expect(equipment.getHardwareFlat()[0].address).toEqual(locations[0].address);
    });
    it('should include the tax rate', () => {
      expect(equipment.getHardwareFlat()[0].tax_rate).toEqual(taxDistricts[0].tax_rate);
    });
    it('should return single hardware if id provided', () => {
      expect((equipment.getHardwareFlat(1)).hardware_id).toEqual(1);
    });
  });
});

