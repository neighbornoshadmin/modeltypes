import Catalog from '../Catalog';

let products;
let catalog;
let categories;
let suppliers;

beforeEach(() => {
  products = [
    { product_id: 0, active: false, supplier_id: 0, category_id: 0 },
    { product_id: 1, active: true, supplier_id: 1, category_id: 1 },
    { product_id: 2, active: true, supplier_id: 1, category_id: 1 },
  ];
  categories = [
    { category_id: 0, name: 'category 0' },
    { category_id: 1, name: 'category 1' },
  ];
  suppliers = [
    { supplier_id: 0, name: 'supplier 0' },
    { supplier_id: 1, name: 'supplier 1' },
  ];
  catalog = new Catalog(products, suppliers, categories);
});

describe('Catalog', () => {
  describe('ctor', () => {
    it('should set products', () => {
      expect(catalog.products).toEqual(products);
    });
    it('should create empty array if products null', () => {
      catalog = new Catalog(null, suppliers, categories);
      expect(catalog.products).toEqual([]);
    });
    it('should create empty array if products undefined', () => {
      catalog = new Catalog(undefined, suppliers, categories);
      expect(catalog.products).toEqual([]);
    });
    it('should set suppliers', () => {
      expect(catalog.suppliers).toEqual(suppliers);
    });
    it('should create empty array if suppliers null', () => {
      catalog = new Catalog(products, null, categories);
      expect(catalog.suppliers).toEqual([]);
    });
    it('should create empty array if suppliers undefined', () => {
      catalog = new Catalog(products, undefined, categories);
      expect(catalog.suppliers).toEqual([]);
    });
    it('should set categories', () => {
      expect(catalog.categories).toEqual(categories);
    });
    it('should create empty array if categories null', () => {
      catalog = new Catalog(products, suppliers, null);
      expect(catalog.categories).toEqual([]);
    });
    it('should create empty array if categories undefined', () => {
      catalog = new Catalog(products, suppliers, undefined);
      expect(catalog.categories).toEqual([]);
    });
  });
  describe('getActiveProducts', () => {
    it('should include only active products', () => {
      expect(catalog.activeProducts).toEqual(products.filter(p => [1, 2].includes(p.product_id)));
    });
  });
  describe('getProductsForStock', () => {
    it('should include stocked products', () => {
      const stock = [{ product_id: 1, quantity: 1 }, { product_id: 2, quantity: 1 }];
      expect(catalog.getProductsForStock(stock))
        .toEqual(products.filter(p => [1, 2].includes(p.product_id)));
    });
    // it('should exclude products with zero quantity', () => {
    //  const stock = [{ product_id: 1, quantity: 0}, { product_id: 2, quantity: 1 }];
  });
  describe('getProduct', () => {
    it('should return correct product', () => {
      expect(catalog.getProduct(1)).toEqual(products[1]);
    });
    it('should exclude supplier if withRelationships false', () => {
      expect((catalog.getProduct(1, false)).supplier).toBeUndefined();
    });
    it('should inlude supplier if withRelationships true', () => {
      expect((catalog.getProduct(1, true)).supplier).toEqual(suppliers[1]);
    });
    it('should exclude category if withRelationships false', () => {
      expect((catalog.getProduct(1, false)).category).toBeUndefined();
    });
    it('should include category if withRelationships true', () => {
      expect((catalog.getProduct(1, true)).category).toEqual(categories[1]);
    });
  });
});
