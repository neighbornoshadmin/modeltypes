import { Guid } from './Guid';

export default interface Hardware {
  HardwareId: Guid;
  LocationId: Guid;
  Name: string;
  SerialNumber: string;
  Notes: string;
  Active: boolean;
  FreeVend: boolean;
  BackendConfig: string;
  StartupScript: string;
  Picture: string;
  PictureFilename: string;
  Config: {
    Active: boolean;
    FreeVend: boolean;
    ScreenOrientation: 'Normal' | 'Left' | 'Right' | 'Inverted';
    BackendVersion: string;
  }
}
