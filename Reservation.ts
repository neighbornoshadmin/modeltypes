import { Guid } from './Guid';
import ReservationStatus from './ReservationStatus';
import PaymentStatus from './PaymentStatus';
import ReservationSource from './ReservationSource';
import ReservationResult from './ReservationResult';
import ReservationProduct from './ReservationProduct';
import ReservationPayment from './ReservationPayment';

export default class Reservation {
  CreatedTime: number;

  HardwareId: Guid;

  ReservationId: Guid;

  UserId: Guid; // If reservation is from machine this will be machine id

  UserEmail: string; // If reservation is from machine this will be machine name

  VoucherCode: string;

  Expires: number;

  PaymentStatus: PaymentStatus;

  Status: ReservationStatus;

  Result: ReservationResult;

  Source: ReservationSource;

  Tip: number;

  TippingCloseTime: number;

  Products: ReservationProduct[];

  Discount: number;

  Subtotal: number;

  Tax: number;

  TaxRate: number;

  Total: number;

  ReceiptRequested: boolean;

  ReceiptEmail: string;

  ReceiptMessageId: string;

  Payment: ReservationPayment;

  Closed: boolean | null;

  Open: boolean | null;
}
